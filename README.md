# Recipe



## Getting started

## Config 
หลังจาก clone project เสร็จแล้ว
ลบข้อความ หรือ ย้ายไฟล์ก่อนจึงจะทำการเปลี่ยน branch ได้ 
เลือกเมนู git -> branches -> origin/master -> Checkout
หลังจากเปลี่ยน branch แล้วให้เลือกเปิดไฟล์ไหนก็ได้ แล้วทำการ Trust Project เพื่อทำการ Sysn

## Tools

* retrofit
* Parcelable
* Room Database
* lifecycle
* Glide

***

## Name
Recipe

## Description
Use [recipes.json](https://hf-android-app.s3-eu-west-1.amazonaws.com/android-test/recipes.json) to build an application that shows a list of recipes via HTTP request. When the app is first launched, fetch the recipes JSON from the API and store it in the app DB.

